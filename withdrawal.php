<?php
require "header.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

include "PHPMailer/src/PHPMailer.php";
include "PHPMailer/src/SMTP.php";
include "PHPMailer/src/Exception.php";

?>
<html>
<head>
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap4.css">

</head>
<body >
<!-- style="background-image: url('images/bg-01.jpg');" -->
<?php
if(isset($_SESSION['username']))
{  

include "controller/user_controller.php";

$user = new User();
$user->setName($_SESSION['username']);

$moneyRaw = $user->getMoney();
$money = number_format($moneyRaw , 0, ',', '.');
?>

<!-- init -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">atm</a></li>
    <li class="breadcrumb-item active" aria-current="page">withdrawal</li>
  </ol>
</nav>

<div class="container" >
<!-- <div class="alert alert-warning" role="alert">
  Hãy nhập đúng số tiền mà bạn muốn nạp, chúng tôi không chịu trách nghiệm cho việc bạn nhập nhiều hơn số tiền!
</div> -->
<h1 class="mx-auto" style="width: 200px;">Rút tiền</h1> 
<form method="post">
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Số dư hiện tại:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $money; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="tien">Số tiền cần rút</label>
    <input type="number" class="form-control" id="tien" name="amount"  placeholder="Nhập số tiền">
    <small id="emailHelp" class="form-text text-muted">Chúng tôi sẽ không lưu lại thông tin này!</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="pass" class="form-control" id="exampleInputPassword1">
    <small id="emailHelp" class="form-text text-muted">Mật khẩu của bạn để bảo mật những dữ liệu này!</small>

  </div>
  <button type="submit" name="rut_tien" class="btn btn-primary">Rút Tiền</button>
</form>
<?php
  if(isset($_POST['rut_tien'])){
    $user->setPass($_POST['pass']);
    if($user->checkAccount()){
    $tien_rut = $_POST['amount'];
    $user->setAmount($tien_rut);
    $checkHasMoney = $user->checkMoney($tien_rut);
    if(!$checkHasMoney){
      echo "Không đủ số tiền mà bạn đã nhập!";
      return ;
    }
    $user-> rut_tien();
    $mail = new PHPMailer(true);
try {
    //Server settings
    $mail->SMTPDebug = 0;                    // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'dangtung789.td@gmail.com';                     // SMTP username
    $mail->Password   = 'wuwxwpcuzjqxswng';                        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;  
    $mail->setFrom('dangtung789.td@gmail.com', 'Mailer');
    $mail->addAddress($_SESSION['username']);              

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Mail Ngan Hang';
    $con_lai = $moneyRaw - $tien_rut; 
    $con_lai = number_format($con_lai , 0, ',', '.');
    $tien_rut = number_format($tien_rut , 0, ',', '.');
    $mail->Body    = "Hôm nay bạn đã rút tiền từ tài khoản, số tiền rút:<b>$tien_rut đ</b> </br> Số tiền còn lại:  <b>$con_lai đ</b>";
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Messhasage  been sent';

} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
header("Refresh:0; url=main.php");
    }
    else{
      echo "Sai mật khẩu!";
    }
  }
?>
</div>


<?php
}
else
{
  header("Refresh: 0; URL=login.php");
?>

<?php
}
?>

</body>
</html>