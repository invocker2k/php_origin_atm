<?php
    // require  ($_SERVER['DOCUMENT_ROOT'].'/atm/config/connect.php');
class Post{
    private $id;
    private $title;
    private $content;
    private $link_img;

    public function setId($id){
        $this->id = $id;
    }

    public function setEmailAccount($id){
        $this->emailAccount = $id;
    }

    public function setAmount($amount){
        $this->amount = $amount;
    }

    public function setTypeTransaction($typeTransaction){
        $this->typeTransaction = $typeTransaction;
    }
    // public function _construct()
    // {
        
    // }

    
    public function getAllPost(){
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="select * from post";
        $run = $mysql -> executeResult($sql);
        $mysql -> dbDisconnect();
        return $run;
    }

    public function getPostById($id){
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="select * from post where id = $id";
        $run = $mysql -> executeResult($sql);
        $mysql -> dbDisconnect();
        return $run;
    }

    public function getAllCategoryPost(){
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="select * from category_post";
        $run = $mysql -> executeResult($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
    public function newCategory($name)
    {
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="INSERT INTO category_post(category_name) VALUES('$name')";
        $run = $mysql -> freeRun($sql);
        $mysql -> dbDisconnect();

        return $run;
    }
    public function newPost($idCategory,$title,$content,$image)
    {
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="INSERT INTO post(category_post_id,title, content, link_img) VALUES('$idCategory','$title','$content','$image')";
        $run = $mysql -> freeRun($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
    
    public function updatePost($id,$idCategory,$title,$content,$image)
    {
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="UPDATE  post set category_post_id = '$idCategory', title = '$title', content = '$content', link_img = '$image' where id = '$id'";
        $run = $mysql -> freeRun($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
    public function deletePost($id)
    {
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="delete from post where id='$id'";
        $run = $mysql ->freeRun($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
    public function deleteCategory($id)
    {
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="delete from category_post where id='$id'";
        $run = $mysql ->freeRun($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
    public function setTransaction(){
        $mysql = new Mysql();
        $mysql -> dbConnect();
        $sql ="insert into d_transaction(idAccount, amount, typeTransaction) values('$this->idAccount','$this->amount','$this->typeTransaction'";
        $run = $mysql -> freeRun($sql);
        $mysql -> dbDisconnect();
        return $run;
    }
}

?>