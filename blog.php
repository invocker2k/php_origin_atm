<?php
    include "header.php";
include "controller/Post.controller.php";
$post = new Post();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">

</head>
<body>
    <div class="container">
    <!-- nav bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Product</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">services</a>
      </li>
      <?php
        $categorys = $post->getAllCategoryPost();
       
      ?>
      <!-- <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Blog
        </a> -->
        <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> -->
          <?php
          foreach( $categorys as  $category){
            ?>
            <li class="nav-item">
            <a class="nav-link" href="blog.php?category=<?= $category['category_name']?>"><?= $category['category_name']?></a>
            </li>
            <?php
          }
          ?>
        <!-- </div> -->
      <?php
      ?>
      <li class="nav-item">
        <a class="nav-link" href="#">About us</a>
      </li>
    </ul>
  </div>


</nav>
<br>
    <!-- content post-->
    <?php

  $allPost = $post -> getAllPost();
  ?>
  <div class="row">
   
    <div class="col-sm-4">
        <?php 
        foreach($allPost as $p){ 
        ?>
          <div id="list-example" class="list-group">
          <a class="list-group-item list-group-item-action" href="blog.php?id=<?= $p['id'];?>"><?= $p['title'];?></a>
          </div>
          <?php    } ?>
      </div>
    <div class="col-sm-8">
      <?php
      if(isset($_GET['id'])){
        $postId = $post -> getPostById($_GET['id']);
        foreach($postId as $p){
          ?>
          <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
          <img src="images/<?= $p['link_img'];?>" alt="Girl in a jacket" width="500" height="600">
          <h2> <?= $p['title'];?></h2>
          <p><?= $p['content'];?></p>
        <?php
          break;
          }
      }
      else{
        foreach($allPost as $p){
          ?>
          <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
          <img src="images/nganhang.jpg" alt="Girl in a jacket" width="500" height="600">
          <h2> <?= $p['title'];?></h2>
          <p><?= $p['content'];?></p>
        <?php
          break;
          }
        }
      ?>
      
   
      </div>
    </div>
  </div>
  
</div>
  
</div>
</body>
</html>