<?php
session_start();
include "controller/Account.controller.php";
?>
<html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">

<!-- <title>Untitled Document</title> -->
</head>

<body>
<?php
// error_reporting(E_ERROR | E_PARSE);


if($_SESSION['username'] == "admin")
{       
        echo '<nav class="navbar navbar-dark bg-primary">' 
        . ' <a class="navbar-brand" href="index.php">ATM</a>'
        . "<div class='float-right'>"
        . "Chào &nbsp;" 
        . " <a href=\"your_profile.php\" class='text-dark'> " . "admin" . "</a> " 
        .  "&nbsp;&nbsp;" 
        . "<a href=\"edit_profile.php\" class='text-dark'>Edit</a> </br>" 
        . "<a href=\"logout.php\" class='text-dark'>Thoát</a>"
        . "</div>"
        . '</nav>';
?>
<?php

//  logic 
    $account = new d_User();
    $allUser = $account -> getInfo();
   
       ?>
<div class="container">
        <br>
        <br>
       <table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">full name</th>
      <th scope="col">date of birth</th>
      <th scope="col">address</th>
      <th scope="col">email</th>
      <th scope="col">Tùy chọn</th>

    </tr>
  </thead>
  <tbody>
  <?php
   foreach($allUser as $user){
       ?>
  <tr>

      <th scope="row"><?php echo $user['id']?></th>
      <td><?php echo $user['full_name']?></td>
      <td><?php echo $user['date_of_birth']?></td>
      <td><?php echo $user['address']?></td>
      <td><?php echo $user['email']?></td>
      <td><a href="/atm/edit_profile.php?email=<?php echo $user['email']?>" class='text-dark'>Edit</a> <a href="del_acc.php?email=<?php echo $user['email']?>" onclick="if(confirm('Xác nhận xóa thông tin'))return true; else return false;" class='text-dark'>Delete</a> </td>


    </tr>
    <?php
    }
?>
  </tbody>
</table>

<?php
}
else
{
  echo '<nav class="navbar navbar-dark bg-primary">' 
  . ' <a class="navbar-brand" href="index.php">ATM</a>'
  . "<div class='float-right'>"
  . "Chào &nbsp;" 
  . " <a href=\"your_profile.php\" class='text-dark'> " . $_SESSION['username'] . "</a> " 
  .  "&nbsp;&nbsp;" 
  . "<a href=\"edit_profile.php\" class='text-dark'>Edit</a> </br>" 
  . "<a href=\"logout.php\" class='text-dark'>Thoát</a>"
  . "</div>"
  . '</nav>';
?>
<div class="alert alert-warning" role="alert">
  Chức năng này chỉ dành cho nhà quản trị!
</div>
<?php
}

?>
</div>
</body>
</html>
