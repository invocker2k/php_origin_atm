<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

include "PHPMailer/src/PHPMailer.php";
include "PHPMailer/src/SMTP.php";
include "PHPMailer/src/Exception.php";
// user controller
include "controller/user_controller.php";

?>
<html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>Untitled Document</title> -->
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>
<?php
require "header.php";
// error_reporting(E_ERROR | E_PARSE);


?>

  </form>

  <!-- content  -->

    <div class="mx-auto" style="width: 50%;">
    <!-- card -->
        <form method="POST">
            <div class="card text-center">
                <div class="card-header">
                    <div class="float-left"><h5 class="card-title">Đổi mật khẩu</h5></div>
                </div>
                <div class="card-body">
                    <!-- <h5 class="card-title">Vui lòng nhập email hoặc số điện thoại để tìm kiếm tài khoản.</h5> -->
                    <p class="card-text">Vui lòng nhập mật khẩu hiện tại.</p>
                    <input class="form-control mr-sm-2" name="oldPass" type="password" placeholder="your password">
                </div>

                <div class="card-body">
                    <label for="tien">Mật khẩu mới </label>
                    <input class="form-control mr-sm-2" name="pass" type="password" placeholder="your password">
                </div>
                <div class="card-body">
                    <label for="tien">Mật lại mật khẩu </label>
                    <input class="form-control mr-sm-2" name="rePass" type="password" placeholder="your password">
                    <!-- <small id="emailHelp" class="form-text text-muted">Chúng tôi sẽ không lưu lại thông tin này!</small> -->
                </div>

                <div class="card-footer text-muted ">
                    <div class="float-right">                   
                     <input class="btn btn-outline my-2 my-sm-0" name="ok" type="submit" value="Đổi mật khẩu" />
                     <button class="btn btn-outline my-2 my-sm-0" type="submit" style="cursor: pointer;"><a href="main.php">Hủy</a></button></div>
                </div>
            </div>
        </form>
        <?php
            if(isset($_POST['ok'])){

                $userEmail = $_SESSION['username'];
                $pass = $_POST['pass'];
                $oldPass = $_POST['oldPass'];
                //  get pass
                $user = new User();
                $user -> setPass($oldPass);
                $user -> setName($userEmail);

                if($user->checkAccount()){
                $user -> setPass($pass);
                $passUser = $user -> changerPass();

                

                // mail
                $mail = new PHPMailer(true);
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                     // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'dangtung789.td@gmail.com';                     // SMTP username
                    $mail->Password   = 'wuwxwpcuzjqxswng';                               // SMTP password
                    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    //Recipients
                    $mail->setFrom('dangtung789.td@gmail.com', 'Mailer');
                    // $mail->addAddress('dangutng789.td@gmail.com', 'Joe User');     // Add a recipient
                    $mail->addAddress($userEmail);               
                    // $mail->addReplyTo('info@example.com', 'Information');// Name is optional
                    // $mail->addCC('cc@example.com');
                    // $mail->addBCC('bcc@example.com');
                    // Attachments
                    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                    // Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Here is the subject';
                    $mail->Body    = "your new pass: $pass</b>";
                    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    $mail->send();
                    echo 'Đổi mật khẩu thành công';
                } catch (Exception $e) {
                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }
            }
        
        else{
            echo "Sai mật khẩu cũ.";
        }
    }
        ?>
    </div>
<?php
?>
</body>
</html>
