<?php
    include "header.php";
include "controller/Post.controller.php";
$post = new Post();
$allPost = $post -> getAllPost();
$categorys = $post->getAllCategoryPost();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">

</head>
<body>
    <div class="container">
    <!-- nav bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="manager_blog.php?ql=baiviet">Quản lý bài viết</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?ql=danhmuc">Quản lý danh mục</a>
      </li>
    </ul>
  </div>


</nav>
<br>
    <!-- content post-->
    <?php
    
  if(isset($_GET['ql'])){
      switch ($_GET['ql']) {
        case 'baiviet':
             ?>
    <a href="manager_blog.php?ql=thembaiviet">Tạo mới một bài viết.</a> 
    <table class="table table-striped table-bordered">
    <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">id của danh mục</th>
      <th scope="col"> Tiêu đề</th>
      <th scope="col">nội dung</th>
      <th scope="col">file ảnh</th>
      <th scope="col">Tùy chọn</th>

    </tr>
  </thead>
  <tbody>
  <?php
   foreach($allPost as $p){
       ?>
  <tr>
      <th scope="row"><?php echo $p['id']?></th>
      <td><?php echo $p['category_post_id']?></td>
      <td><?php echo $p['title']?></td>
      <td>Nhấn edit để sửa nội dung.</td>
      <td>
      <img src="images/<?php echo $p['link_img']?>" alt="Girl in a jacket" width="50" height="60">

      </td>
      
      <td><a href="manager_blog.php?ql=updatePost&id=<?php echo $p['id']?>" class='text-dark'>Edit</a> <a href="delete-post.php?id=<?php echo $p['id']?>" onclick="if(confirm('Xác nhận xóa bài biết này?'))return true; else return false;" class='text-dark'>Delete</a> </td>


    </tr>
    <?php
    }
    ?>
    </tbody>
    </table>
    <?php
              break;
       case 'updatePost':

       $postId = $post->getPostById($_GET['id']);
        foreach($postId as $p){
       ?>
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Sửa bài viết.</h1>
      <hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <img src="images/<?= $p['link_img'];?>" class="avatar img-circle" width="400px" height="auto" alt="avatar">
          <h6>Upload a different photo...</h6>

          <input type="file" name="imgPost" class="form-control">
        </div>
      </div>
      
      <!-- edit form column -->
      <div class="col-md-12  personal-info">
        
        <h3>Thông tin bài đăng.</h3>
        
        <div class="form-group">
            <label class="col-lg-3 control-label">id Danh mục</label>
            <div class="col-lg-8">
              <input class="form-control" name ="idDM" type="text" value="<?= $p['category_post_id'];?>">
            </div>
          </div>
        
          <div class="form-group">
            <label class="col-lg-3 control-label">Title:</label>
            <div class="col-lg-8">
              <input class="form-control" name="title" type="text" value="<?= $p['title'];?>"> 
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Content:</label>
            <div class="col-lg-8">
              <input class="form-control" name="content" type="text" value="<?= $p['content'];?>" >
            </div>
          </div>
          <button type="submit" name="newPost" class="btn btn-primary mb-2">Confirm</button>
          
   <?php
        }
        if(isset($_POST['newPost'])){
            $file_name = $_FILES['imgPost']['name'];
            move_uploaded_file($_FILES['imgPost']['tmp_name'], "images/".$_FILES['imgPost']['name']);
            $idCategory = $_POST['idDM'];
            $title = $_POST['title'];
            $content = $_POST['content'];

            $post -> updatePost($_GET['id'],$idCategory,$title,$content,$file_name);
        }
            break;
        // case danh muc
       case 'danhmuc':
            ?>
            <a href="manager_blog.php?ql=themdanhmuc">Tạo một danh mục mới.</a> 
    
    <table class="table table-striped table-bordered">
    <thead class="thead-dark">
    <tr>
      <th scope="col">id của danh mục.</th>
      <th scope="col">Tên danh mục.</th>
      <th scope="col">Chỉnh sửa.</th>

    </tr>
  </thead>
  <tbody>
  <?php
   foreach($categorys as $category){
       ?>
  <tr>
      <th scope="row"><?php echo $category['id']?></th>
      <td><?php echo $category['category_name']?>
      </td>
      
      <td><a href="manager_blog.php?ql=updatePost&id=<?php echo $p['id']?>" class='text-dark'>Edit</a> <a href="delete-post.php?idDm=<?php echo $category['id']?>" onclick="if(confirm('Xác nhận xóa danh mục này?'))return true; else return false;" class='text-dark'>Delete</a> </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
    </table>
            <?php
            break;
        // case them danh muc
        case "themdanhmuc":
        ?>
        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Thêm bài viết mới.</h1>
      <hr>
	<div class="row">
      
      <!-- edit form column -->
      <div class="col-md-12  personal-info">
        
        <h3>Thông tin danh mục.</h3>

        
          <div class="form-group">
            <label class="col-lg-3 control-label">Tên danh mục:</label>
            <div class="col-lg-8">
              <input class="form-control" name="name" type="text">
            </div>
          </div>
          <button type="submit" name="newCategory" class="btn btn-primary mb-2">Confirm</button>
          
   <?php
        if(isset($_POST['newCategory'])){
            
            
            $name = $_POST['name'];

            $post -> newCategory($name);
        }
    break;
        case 'thembaiviet':
            ?>
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Thêm bài viết mới.</h1>
      <hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>

          <input type="file" name="imgPost" class="form-control">
        </div>
      </div>
      
      <!-- edit form column -->
      <div class="col-md-12  personal-info">
        
        <h3>Thông tin bài đăng.</h3>
        
        <div class="form-group">
        <select name="idDM" class="custom-select custom-select-lg mb-3">
            <option selected value ="">Chọn một danh mục</option>
            <?php
          foreach( $categorys as  $category){
            ?>
            <option  value="<?= $category['id']?>"><?= $category['category_name']?></option>
            <?php
          }
          ?>
          </select>
          </div>
        
          <div class="form-group">
            <label class="col-lg-3 control-label">Title:</label>
            <div class="col-lg-8">
              <input class="form-control" name="title" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Content:</label>
            <div class="col-lg-8">
              <input class="form-control" name="content" type="textarea" >
            </div>
          </div>
          <button type="submit" name="newPost" class="btn btn-primary mb-2">Confirm</button>
          
   <?php
        if(isset($_POST['newPost'])){
            $file_name = $_FILES['imgPost']['name'];
            move_uploaded_file($_FILES['imgPost']['tmp_name'], "images/".$_FILES['imgPost']['name']);
            $idCategory = $_POST['idDM'];
            $title = $_POST['title'];
            $content = $_POST['content'];

            $post -> newPost($idCategory,$title,$content,$file_name);
        }
            break;
        default:
              # code...
            break;
      }
     
  }
  ?>
    
</div>
  
</div>
</body>
</html>