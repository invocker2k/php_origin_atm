
<html>
<head>
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
</head>
<body >
<!-- style="background-image: url('images/bg-01.jpg');" -->
<?php 
require "header.php";
if(isset($_SESSION['username']))
{

?>

<!-- init -->
<div class="container" style="margin-top: 40px" >
  <div class="row justify-content-md-center" >
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/nap_tien.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">nạp tiền</h5>
            <p class="card-text">Nạp tiền vào tài khoản của bạn.</p>
            <a href="/atm/nap_tien.php" class="btn btn-primary">Go money into the account</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/list_user.jfif" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">List User</h5>
            <p class="card-text">Manager user (only admin).</p>
            <a href="/atm/list-user.php" class="btn btn-primary">Danh sách người dùng</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/truy_van_so_du.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Truy vấn số dư</h5>
            <p class="card-text">Kiểm tra số dư trong tài khoản.</p>
            <a href="#" class="btn btn-primary">Tới truy vấn số dư</a>
          </div>
      </div>
    </div>
  </div>

  <!-- ---------------------row 2 ------------------------- -->
  <!-- ---------------------row 2 ------------------------- -->
  </br>
  <div class="row">
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/chuyen_tien.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Nạp tiền</h5>
            <p class="card-text">Nạp tiền nhiều mệnh giá.</p>
            <a href="/atm/nap_tien2.php" class="btn btn-primary">Nạp</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/quan_ly_noi_dung.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Quản lý nội dung</h5>
            <p class="card-text">Quản lý bài viết tin tức.</p>
            <a href="/atm/manager_blog.php" class="btn btn-primary">Quản lý nội dung</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/thanh_toan.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Thanh Toán</h5>
            <p class="card-text">Hóa đơn điện tử onlile.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
      </div>
    </div>
  </div>
  <!-- DONE CONTAINER  -->
</div>



<?php

}
else
{
echo "<a href=\"login.php\">Đăng nhập</a>";echo "&nbsp;&nbsp;";
echo "<a href=\"register.php\">Đăng ký</a>";
?>

<?php
}
?>

</body>
</html>