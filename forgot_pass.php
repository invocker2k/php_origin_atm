<?php
include "controller/Account.controller.php";

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

include "PHPMailer/src/PHPMailer.php";
include "PHPMailer/src/SMTP.php";
include "PHPMailer/src/Exception.php";
// user controller
include "controller/user_controller.php";

?>
<html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>Untitled Document</title> -->
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>
<?php
// error_reporting(E_ERROR | E_PARSE);
session_start();
if(isset($_SESSION['username']))
{
echo "Chào &nbsp;".$_SESSION["username"]; echo "&nbsp;&nbsp;";
echo "<a href=\"logout.php\">Thoát</a>";
}
else
{
?>
    <nav class="navbar navbar-dark bg-primary">
    <a class="navbar-brand">ATM</a>
  <form class="form-inline" method="POST">
    <input class="form-control mr-sm-2" name='username' type="text" placeholder="Username" >
    <input class="form-control mr-sm-2" type="password" name='pass' placeholder="Password">

    <input class="btn" type="submit" name='login' value="login"/>
  </form>
        <?php
        if (isset($_POST['login']))
        {	
            // get info user from client
            $email_user = $_POST['username'];
            $pass_user = $_POST['pass'];
            // create user Controller
            $user = new User();
            $isUser = $user -> user_login($email_user, $pass_user);
            if ($isUser == 1)
            {
                // set session
                $_SESSION['username'] = $email_user;
                $_SESSION['password'] = $pass_user;
                // reload browser in 3 second to main.php
                header("Refresh: 3; URL=main.php");
                echo "Đăng nhập thành công. Bạn đang được chuyển đến trang chủ!<br>";
                echo "(Nếu thấy lâu có thể, <a href=\"main.php\">nhấn vào đây</a>)";
            }
            // user not exits
            else{
                echo "Đăng nhập không thành công. Tài khoản hoặc mật khẩu không tồn tại! </br>";
                echo "(Nếu muốn đăng kí 1 tài khoản, <a href=\"register.php\">nhấn vào đây</a>)";
            }
            }
        ?>
  </nav>

  <!-- content  -->

    <div class="mx-auto" style="width: 50%;">
    <!-- card -->
        <form method="POST">
            <div class="card text-center">
                <div class="card-header">
                    <div class="float-left"><h5 class="card-title">Tìm tài khoản của bạn</h5></div>
                </div>
                <div class="card-body">
                    <!-- <h5 class="card-title">Vui lòng nhập email hoặc số điện thoại để tìm kiếm tài khoản.</h5> -->
                    <p class="card-text">Vui lòng nhập email hoặc số điện thoại để tìm kiếm tài khoản.</p>
                    <input class="form-control mr-sm-2" name="email" type="text" placeholder="Email">

                </div>
                <div class="card-footer text-muted ">
                    <div class="float-right">                   
                     <button class="btn btn-outline my-2 my-sm-0" name="ok" type="submit">Tìm kiếm</button>
                     <button class="btn btn-outline my-2 my-sm-0" type="submit" style="cursor: pointer;"><a href="login.php">Hủy</a></button></div>
                </div>
            </div>
        </form>
        <?php
            if(isset($_POST['ok'])){
                $userEmail = $_POST['email'];
                //  get pass
                $user = new User();
                $passUser = $user::getPass($userEmail);
                // mail
                $mail = new PHPMailer(true);
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                      // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'dangtung789.td@gmail.com';                     // SMTP username
                    $mail->Password   = 'wuwxwpcuzjqxswng';                               // SMTP password
                    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    //Recipients
                    $mail->setFrom('dangtung789.td@gmail.com', 'Mailer');
                    // $mail->addAddress('dangutng789.td@gmail.com', 'Joe User');     // Add a recipient
                    $mail->addAddress($userEmail);               
                    // $mail->addReplyTo('info@example.com', 'Information');// Name is optional
                    // $mail->addCC('cc@example.com');
                    // $mail->addBCC('bcc@example.com');
                    // Attachments
                    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                    // Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Here is the subject';
                    $mail->Body    = "your pass: $passUser</b>";
                    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    $mail->send();
                    echo 'Lấy mật khẩu thành công, kiểm tra lại email bạn nhé!';
                } catch (Exception $e) {
                    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }
            }
        ?>
    </div>
<?php
}
?>
</body>
</html>
