
<?php
require "header.php";
?>
<html>
<head>
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body >
<!-- style="background-image: url('images/bg-01.jpg');" -->
<?php

if(isset($_SESSION['username']))
{

?>

<!-- init -->
<div class="container" >
  <div class="row justify-content-md-center" >
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/cash-withdrawal-icon-png_239820.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Rút tiền</h5>
            <p class="card-text">Rút tiền trong tài khoản của bạn.</p>
            <a href="withdrawal.php" class="btn btn-primary">Đi tới rút tiền</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/change_pass2.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Đổi mật khẩu</h5>
            <p class="card-text">Dổi lại mật khẩu mới.</p>
            <a href="change_pass.php" class="btn btn-primary">Chuyển tới đổi mật khẩu</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/truy_van_so_du.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Truy vấn số dư</h5>
            <p class="card-text">Kiểm tra số dư trong tài khoản.</p>
            <a href="balance_inquiry.php" class="btn btn-primary">Tới truy vấn số dư</a>
          </div>
      </div>
    </div>
  </div>

  <!-- ------------------------Row 3 ----------------------------- -->
</br>
  <div class="row">
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/user_info_2.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Thông tin chủ tài khoản</h5>
            <p class="card-text">Thông tin chi tiết chủ tài khoản.</p>
            <a href="your_profile.php" class="btn btn-primary">Xem thông tin tài khoản</a>
          </div>
      </div>
    </div>
    <div class="col-sm">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" src="images/news.jpg" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Tin tức</h5>
            <p class="card-text">Cập nhật thông tin chính thức từ ngân hàng.</p>
            <a href="blog.php" class="btn btn-primary">Xem tin tức</a>
          </div>
      </div>
    </div>
    <div class="col-sm ">
        <div class="card bg-info text-white border-warning" style="width: 18rem;">
          <img class="card-img-top" height="300px" src="images/other_icon.webp" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Dịch vụ khác</h5>
            <p class="card-text">Một số dịch vụ khác.</p>
            <a href="/atm/khac.php" class="btn btn-primary">Các dịch vụ khác</a>
          </div>
      </div>
    </div>
  </div>


  <!-- DONE CONTAINER  -->
</div>



<?php

}
else
{
 
?>

<?php
}
?>

</body>
</html>