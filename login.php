<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
<!--===============================================================================================-->
</head>
<body>
	<form method="POST"> 
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>
					<h1 class="login100-form-title p-b-34 p-t-27">ATM</h1>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div class="container-login100-form-btn">
						<input class="login100-form-btn" name="user_login" type="submit" value="Login">
					</div>

<?php
include "controller/Account.controller.php";
if(!isset($_SESSION)) session_start();
include "controller/user_controller.php";
if (isset($_POST['user_login']))
{	
	// get info user from client
	 $email_user = $_POST['username'];
	 $pass_user = $_POST['pass'];
	 // create user Controller
	 $user = new User();
	 $isUser = $user -> user_login($email_user, $pass_user);
     if ($isUser == 1)
     {
		 // set session
          $_SESSION['username'] = $email_user;
		  $_SESSION['password'] = $pass_user;
		  // reload browser in 3 second to main.php
          header("Refresh: 2; URL=main.php");
          echo "Đăng nhập thành công. Bạn đang được chuyển đến trang chủ!<br>";
          echo "(Nếu thấy lâu có thể, <a href=\"main.php\">nhấn vào đây</a>)";
	 }
	 // user not exits
	 else{
		  echo "</br><h5 style='color:red'>Đăng nhập không thành công. Tài khoản hoặc mật khẩu không tồn tại!</h5> </br>";
		  echo "<p style='color:#ffffff'>(Nếu muốn đăng kí 1 tài khoản, <a href=\"register.php\">nhấn vào đây</a>)</p>";
	 }
	}
?>

					<div class="text-center p-t-90">
					Bạn chưa có tài khoản?<a class="txt1" href="register.php">
							 Đăngkí
						</a>
						<br>
						<a class="txt1" href="forgot_pass.php">
							Forgot Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	</form>

	<div id="dropDownSelect1"></div>


<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>