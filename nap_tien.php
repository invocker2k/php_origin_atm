<?php
include "controller/user_controller.php";
    // Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

include "PHPMailer/src/PHPMailer.php";
include "PHPMailer/src/SMTP.php";
include "PHPMailer/src/Exception.php";
?>

<html>
<head>
    <title>Index</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="css/nap_tien.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
</head>
<body >

<?php
  require "header.php";
if(isset($_SESSION['username']))
{

?>

<!-- init -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">atm</a></li>
    <li class="breadcrumb-item active" aria-current="page">naptien</li>
  </ol>
</nav>
<div class="container" >
<div class="alert alert-warning" role="alert">
  Hệ thống tích hợp bảo mật theo tiêu chuẩn quốc tế, giúp bạn an toàn giao dịch!
</div>
 NẠP TIỀN </br> <br>

 <form method="post">
<!-- start -->
<h6>Chọn mệnh giá</h6>
<div class="row">
		
    <div class="custom-radios">
      <div>
        <input type="radio" id="color-1" name="color" value="50000" checked>
        <label for="color-1">
          <span>
            50000
          </span>
        </label>
      </div>
      
      <div>
        <input type="radio" id="color-2" name="color" value="100000">
        <label for="color-2">
          <span>
            100.000
          </span>
        </label>
      </div>
      
      <div>
        <input type="radio" id="color-3" name="color" value="200000">
        <label for="color-3">
          <span>
            200.000
          </span>
        </label>
      </div>
    
      <div>
        <input type="radio" id="color-4" name="color" value="500000">
        <label for="color-4">
          <span>
            500.000
          </span>
        </label>
      </div>
    </div>
      </div>

<!-- end -->
<!-- so luong -->
<br>
<h6>Số lượng</h6>
    <div class="row">
        <div class="col-sm-3">
            <div class="input-group">
                <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
              <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-dash-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
              <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
            </svg>
                </button>
                </span>
                <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
                <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
              <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-plus-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
              </svg>
                </button>
                </span>
            </div>
        </div>
    </div>
    <br>
<!-- done so luong -->

  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="pass" class="form-control" id="exampleInputPassword1">
    <small id="emailHelp" class="form-text text-muted">Mật khẩu của bạn để bảo mật những dữ liệu này!</small>

  </div>
  <input type="submit" onclick="phep_thuat()" name="add_gold" value="Nạp Tiền" class="btn btn-primary"/>
</form>
<script>
    function phep_thuat(){

       
        }
    
  </script>
<?php

//  xu ly 

$user = new User();
$user->setName($_SESSION['username']);
  if(isset($_POST['add_gold'])){
    $user->setPass($_POST['pass']);
    if(!$user->checkAccount()){
      echo "Sai mật khẩu!";
      return;
    } 
$menh_gia =0;
    // soluong
    $so_luong =  (float) $_POST['quant'][1];
    //  menh gia
        if(!empty($_POST['color'])) {
          $menh_gia = (float) $_POST['color'];
      } else {
          echo 'Please select the value.';
    }
   
    $user = new User();
    $user->setName($_SESSION['username']);
    $user->setAmount($so_luong * $menh_gia);
    $resultNapTien = $user->nap_tien();

    // gui mail

$mail = new PHPMailer(true);
try {
    //Server settings
    $mail->SMTPDebug = 0;
    // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'dangtung789.td@gmail.com';                     // SMTP username
    $mail->Password   = 'wuwxwpcuzjqxswng';                        // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    //Recipients
    $mail->setFrom('dangtung789.td@gmail.com', 'Mailer');
    $mail->addAddress($_SESSION['username']);              

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Mail Ngan Hang';
    $tien =  $so_luong * $menh_gia;
    $tien = number_format($tien , 0, ',', '.');
    $mail->Body    = "Hôm nay bạn đã nạp tiền vào tài khoản số tiền: <b>$tien đ</b>";
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Đã nạp tiền vào tài khoản';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
  }
?>

</div>
<?php

}
else
{
echo "<a href=\"login.php\">Đăng nhập</a>";echo "&nbsp;&nbsp;";
echo "<a href=\"register.php\">Đăng ký</a>";
?>

<?php
}
?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="js/nap_tien.js"></script>
</body>
</html>