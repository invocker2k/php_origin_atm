-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2020 at 08:45 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atm`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `remaining_amount` varchar(255) DEFAULT NULL,
  `nicname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `remaining_amount`, `nicname`, `password`) VALUES
(1, '15426785', 'dangtung789.td@gmail.com', 'q'),
(2, NULL, 'admin', 'admin'),
(3, NULL, 'ngoctrinh@g  mail.com', 'ngoc'),
(5, '400000', 'invocker789.td@gmail.com', 'Q');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `id` int(11) NOT NULL,
  `category_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`id`, `category_name`) VALUES
(1, 'Ngân hàng'),
(2, 'Kinh tế'),
(5, 'Hoa hồng'),
(6, 'name danh mục');

-- --------------------------------------------------------

--
-- Table structure for table `d_transaction`
--

CREATE TABLE `d_transaction` (
  `id` int(11) NOT NULL,
  `emailAccount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `typeTransaction` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_transaction`
--

INSERT INTO `d_transaction` (`id`, `emailAccount`, `amount`, `typeTransaction`, `date`) VALUES
(1, 'dangtung789.td@gmail.com', '300000', 'Nạp tiền vào tài khoản', '2020-12-07 15:56:14'),
(2, 'dangtung789.td@gmail.com', '200000', 'Nạp tiền vào tài khoản', '2020-12-07 15:56:44'),
(3, 'dangtung789.td@gmail.com', '5000000', 'Nạp tiền vào tài khoản', '2020-12-07 15:59:53'),
(4, 'dangtung789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-07 16:05:24'),
(5, 'dangtung789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-07 16:05:48'),
(6, 'dangtung789.td@gmail.com', '50000', 'Nạp tiền vào tài khoản', '2020-12-07 16:10:17'),
(7, 'dangtung789.td@gmail.com', '123213', 'Rút tiền từ tài khoản', '2020-12-07 16:12:31'),
(8, 'dangtung789.td@gmail.com', '1000000', 'Rút tiền từ tài khoản', '2020-12-07 16:20:28'),
(9, 'invocker789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-14 06:55:21'),
(10, 'invocker789.td@gmail.com', '100000', 'Rút tiền từ tài khoản', '2020-12-14 06:55:50'),
(11, 'dangtung789.td@gmail.com', '100000', 'Rút tiền từ tài khoản', '2020-12-17 13:36:51'),
(12, 'dangtung789.td@gmail.com', '1', 'Rút tiền từ tài khoản', '2020-12-17 13:46:18'),
(13, 'dangtung789.td@gmail.com', '50000', 'Nạp tiền vào tài khoản', '2020-12-17 13:46:59'),
(14, 'dangtung789.td@gmail.com', '1000000', 'Nạp tiền vào tài khoản', '2020-12-17 13:48:10'),
(15, 'dangtung789.td@gmail.com', '50000', 'Nạp tiền vào tài khoản', '2020-12-17 14:03:38'),
(16, 'dangtung789.td@gmail.com', '1', 'Rút tiền từ tài khoản', '2020-12-17 14:04:47'),
(17, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:19:07'),
(18, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:19:09'),
(19, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:20:15'),
(20, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:22:28'),
(21, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:24:12'),
(22, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:25:13'),
(23, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:25:35'),
(24, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:25:43'),
(25, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:26:00'),
(26, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:26:24'),
(27, 'dangtung789.td@gmail.com', '0', 'Nạp tiền vào tài khoản', '2020-12-17 14:28:46'),
(28, 'dangtung789.td@gmail.com', '600000', 'Nạp tiền vào tài khoản', '2020-12-17 14:29:51'),
(29, 'dangtung789.td@gmail.com', '550000', 'Nạp tiền vào tài khoản', '2020-12-17 14:32:14'),
(30, 'dangtung789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-17 14:32:44'),
(31, 'dangtung789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-17 14:33:58'),
(32, 'dangtung789.td@gmail.com', '500000', 'Nạp tiền vào tài khoản', '2020-12-17 14:34:09'),
(33, 'dangtung789.td@gmail.com', '1000000', 'Nạp tiền vào tài khoản', '2020-12-17 14:38:05'),
(34, 'dangtung789.td@gmail.com', '1000000', 'Nạp tiền vào tài khoản', '2020-12-17 14:38:31'),
(35, 'dangtung789.td@gmail.com', '1150000', 'Nạp tiền vào tài khoản', '2020-12-17 14:39:00'),
(36, 'dangtung789.td@gmail.com', '1050000', 'Nạp tiền vào tài khoản', '2020-12-17 14:39:37'),
(37, 'dangtung789.td@gmail.com', '1050000', 'Nạp tiền vào tài khoản', '2020-12-17 14:43:32'),
(38, 'dangtung789.td@gmail.com', '1050000', 'Nạp tiền vào tài khoản', '2020-12-17 14:43:52');

-- --------------------------------------------------------

--
-- Table structure for table `d_user`
--

CREATE TABLE `d_user` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `date_of_birth` char(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_user`
--

INSERT INTO `d_user` (`id`, `full_name`, `date_of_birth`, `address`, `email`) VALUES
(1, 'Đặng Thanh Tùng', '28-3-2000', 'address', 'dangtung789.td@gmail.com'),
(2, 'a đờ min ', '28-3-2000', 'HN', 'admin'),
(3, 'ngoc trinh ', '28-3-2001', 'HN', 'ngoctrinh@gmail.com'),
(6, 'Nam', '2020-12-16', 'HCM', 'invocker879.td@gmail.com'),
(7, 'NAM BAC', '', 'a', 'invocker789.td@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `category_post_id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `link_img` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `category_post_id`, `title`, `content`, `link_img`) VALUES
(1, 1, 'Dragon Capital không còn là cổ đông lớn của  ', 'Cụ thể, hôm 30/11, Dragon Capital đã thông qua quỹ thành viên Amersham Industries Limited bán 1 triệu cổ phiếu MBB, giảm số lượng sở hữu của nhóm quỹ xuống 137,7 triệu cp, tương đương 4,97% vốn điều lệ của ngân hàng.Theo đó, nhóm quỹ Dragon Capital không còn là cổ đông lớn của MB kể từ ngày 2/12.Chốt phiên 30/11, giá cổ phiếu MBB đứng ở mức 20.350 đồng/cp. Ước tính theo mức giá này, Dragon Capital đã thu về hơn 20,3 tỷ đồng.Bên cạnh Dragon Capital, Công ty TNHH MTV Đầu tư SCIC (SIC) cũng đang đăng ký bán toàn bộ hơn 28.700 cổ phiếu MBB kaf SIC sở hữu trong thời gian từ ngày 26/11 đến ngày 25/12, theo phương thức khớp lệnh và thỏa thuận trên sàn. Tính đến ngày 30/9/2020, tổng tài sản của MBB tăng 4% so với đầu năm, đạt 421.175 tỷ đồng. Dư nợ cho vay khách hàng tăng 7%, tiền gửi khách hàng giảm 1,3%. 9 tháng đầu năm 2020, lợi nhuận trước thuế hợp nhất của ngân hang đạt 8.134 tỉ đồng, tăng 6,8% so với cùng kì 2019 và thực hiện được 81% kế hoạch cả năm 2020.', 'nganhang.jpg'),
(2, 1, 'VDSC: Lợi nhuận Vietcombank năm 2020 có thể tăng trưởng âm do tăng mạnh chi phí dự phòng', 'Công ty chứng khoán Rồng Việt (VDSC) vừa có báo cáo phân tích về ngân hàng Vietcombank (VCB). \r\n\r\nVDSC cho rằng triển vọng năm 2020/2021 của ngân hàng vẫn là lạc quan nhờ kiểm soát dịch bệnh và tăng vốn. \r\n\r\nQuý cuối năm 2020 có thể sẽ chứng kiến một kết quả tích cực đáng kể nhờ ghi nhận phí trả trước. Nhóm phân tích ước tính lợi nhuận trước thuế (LNTT) tăng trưởng 23% trong quý cuối năm, trong đó thu nhập bất thường đóng góp 22%. Cho năm 2020, VDSC ước tính LNTT tăng trưởng âm 2%, trong khi thu nhập hoạt động tăng nhẹ 5%. Điều này là do chi phí hoạt động tăng 6% và chi phí dự phòng tăng 24%. Nhìn chung, khoản phí trả trước chỉ giúp bù đắp một phần sự giảm tốc của hoạt động cho vay chính và thu nhập ngoài lãi. \r\n\r\nNIM ước tính giảm còn 2,9%. Nợ xấu có thể ổn định ở mức 1,0%, nhờ vào việc xóa nợ bù đắp cho tỉ lệ hình thành nợ xấu mới. Tỷ lệ chi phí tín dụng trên dư nợ cho vay được dự đoán là 1,1%, tương đương với việc chi phí dự phòng tăng 24%, giữ tỷ lệ bao phủ nợ xấu trên 200%. ', 'vietcombank.jpg'),
(6, 1, 'NGÀNH NGHỀ KINH DOANH VÀ ĐỊA BÀN KINH DOANH', 'Ngành nghề kinh doanh: Hoạt động chính của Ngân hàng TMCP Công Thương Việt Nam là thực hiện các giao dịch ngân hàng bao gồm huy động và nhận tiền gửi ngắn hạn, trung hạn và dài hạn từ các tổ chức và cá nhân; cho vay ngắn hạn, trung hạn và dài hạn đối với các tổ chức và cá nhân trên cơ sở tính chất và khả năng nguồn vốn của Ngân hàng; thanh toán giữa các tổ chức và cá nhân; thực hiện các giao dịch ngoại tệ, các dịch vụ tài trợ thương mại quốc tế, chiết khấu thương phiếu, trái phiếu, các giấy tờ có giá khác và các dịch vụ ngân hàng khác được Ngân hàng Nhà nước Việt Nam cho phép.  Địa bàn hoạt động: Ngân hàng TMCP Công Thương Việt Nam có trụ sở chính đặt tại số 108 Trần Hưng Đạo, Quận Hoàn Kiếm, Hà Nội, 155 chi nhánh trải dài trên 63 tỉnh, thành phố trên cả nước, có 02 văn phòng đại diện ở Thành phố Hồ Chí Minh và Thành phố Đà Nẵng, 01 Trung tâm Tài trợ thương mại, 05 Trung tâm Quản lý tiền mặt, 03 đơn vị sự nghiệp (Trung tâm thẻ, Trung tâm công nghệ Thông tin, Trường Đào tạo & Phát triển Nguồn nhân lực VietinBank) và 958 phòng giao dịch. Bên cạnh đó, VietinBank có 02 chi nhánh tại CHLB Đức, 01 văn phòng đại diện tại Myanmar và 01 Ngân hàng con ở nước CHDCND Lào (với 01 Trụ sở chính, 01 chi nhánh Champasak, 01 phòng giao dịch Viêng Chăn). Ngoài ra, VietinBank còn có quan hệ với trên 1.000 ngân hàng đại lý tại hơn 90 quốc gia và vùng lãnh thổ trên toàn thế giới.', 'giay-da-nam-cong-so-05.jpg'),
(10, 0, 'hihi', 'z', 'change_pass2.png'),
(11, 0, 'abc', 'xyz', 'giay-da-nam-cong-so-05.jpg'),
(13, 0, 'asa', 'ádasd', 'giay-da-nam-cong-so-06.jpg'),
(15, 2, '123', '123123', 'giay-da-nam-cong-so-05.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `id` int(11) NOT NULL,
  `id_user` int(10) DEFAULT NULL,
  `id_account` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_transaction`
--
ALTER TABLE `d_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_user`
--
ALTER TABLE `d_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_account` (`id_account`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category_post`
--
ALTER TABLE `category_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `d_transaction`
--
ALTER TABLE `d_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `d_user`
--
ALTER TABLE `d_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_account`
--
ALTER TABLE `user_account`
  ADD CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `d_user` (`id`),
  ADD CONSTRAINT `user_account_ibfk_2` FOREIGN KEY (`id_account`) REFERENCES `account` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
