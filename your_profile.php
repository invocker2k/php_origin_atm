<?php

?>
<html>
<head>
</head>
<body>
<style>
.card {
  
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 600px;
  margin: auto;
  text-align: center;
}

.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
</style>
<!--  dont style -->
     <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php
    require "header.php";

    //  header should check session of user[email, pass]
    // show content of main
    if(isset($_SESSION['username'])){

         // user detail controller
         include "controller/user_detail.controller.php";
         include "controller/user_controller.php";

         $uEmail = $_SESSION['username'];

        $full_nameI = '';
        $birdayI  = '';
        $emailI = '';

        $dang_ki = new DangKi();
        $user = new User();
        $user ->setName($uEmail);
        $tien = $user->getMoney();
        $_info = $dang_ki -> GetInfo($uEmail);
      
        $full_nameI = $_info['full_name'];
        $birdayI  = $_info['date_of_birth'];
        $emailI = $_info['email'];
        $address = $_info['address'];
   
?>
<br>
<div class="card">
  <h5>Thông tin tài khoản</h5>
  <h1>Name: <?php echo $full_nameI ?></h1>
  <p class="title"> Số dư tài khoản: <?=number_format($tien , 0, ',', '.');?>đ</p>
  <p>birday:<?php echo $birdayI ?></p>
  <p>email:<?php echo $emailI ?></p>
  <div style="float:left">
    <a href="#" style=" padding: 10px;"><i class="fa fa-dribbble"></i></a>
    <a href="#" style=" padding: 10px;"><i class="fa fa-twitter"></i></a>
    <a href="#" style=" padding: 10px;"><i class="fa fa-linkedin"></i></a>
    <a href="#" style=" padding: 10px;"><i class="fa fa-facebook"></i></a>
  </div>
  
</div> 

<?php

    }
?>
</body>
</html>