<?php
require "header.php";

?>
<html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">

<!-- <title>Untitled Document</title> -->
</head>

<body>

<?php
// error_reporting(E_ERROR | E_PARSE);

include "controller/Transaction.controller.php";

?>
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Truy vấn số dư khách hàng!</h4>
<?php

//  logic 
    $Transaction = new Transaction();
    $Transaction->setEmailAccount($_SESSION['username']);
    $listTrans = $Transaction -> getTransaction();
       ?>
<div class="container mb-0">
        <br>
        <br>
       <table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Stt</th>
      <th scope="col">Email Account</th>
      <th scope="col">Amount</th>
      <th scope="col">Tpe Transaction</th>
      <th scope="col">Transaction time</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $count = 1;
   foreach($listTrans as $trans){
       ?>
  <tr>

      <th scope="row"><?=$count?></th>
      <td><?php echo $trans['emailAccount']?></td>
      <td><?php echo number_format($trans['amount'] , 0, ',', '.');?> đ</td>
      <td><?php echo $trans['typeTransaction']?></td>
      <td><?php echo $trans['date']?></td>
    </tr>
    <?php
    $count ++;
    }
?>
  </tbody>
</table>

<?php
?>
</div>
</div>

</body>
</html>
