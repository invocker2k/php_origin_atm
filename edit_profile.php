<html>
<head>
<title>edit profile</title>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- --------------------------- css --------------------------------------------->
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css">
 <!-- Required meta tags -->
</head>
<body>
<?php
    //  header should check session of user[email, pass]
    include "header.php";
 
    // show content of main
    if(isset($_SESSION['username'])){

         // user detail controller
        include "controller/user_detail.controller.php";
        if(isset($_GET['email'])){
          $dang_ki = new DangKi();
        $_info = $dang_ki -> GetInfo($_GET['email']);

        $full_nameI = $_info['full_name'];
        $birdayI  = $_info['date_of_birth'];
        $emailI = $_info['email'];
        $address = $_info['address'];
        }
        else{
        $uEmail = $_SESSION['username'];
        
        $dang_ki = new DangKi();
        $_info = $dang_ki -> GetInfo($uEmail);
        if($_info == 0){
          $full_nameI = "Cập nhật tên";
          $birdayI  = " cập nhật ngày sinh";
          $emailI ="Cập nhật email";
          $address = "Cập nhật địa chỉ của bạn.";

         
        }
        else{
          $full_nameI = $_info['full_name'];
          $birdayI  = $_info['date_of_birth'];
          $emailI = $_info['email'];
          $address = $_info['address'];
        }
        
        }
?>

<div class="container bootstrap snippets bootdey">
<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Edit Profile</h1>
      <hr>
	<div class="row">
      <!-- left column -->
      <!-- <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>

          <input type="file" name="txtpic" class="form-control">
        </div>
      </div> -->
      
      <!-- edit form column -->
      <div class="col-md-12  personal-info">
        <div class="alert alert-info alert-dismissable">
          <a class="panel-close close" data-dismiss="alert">×</a> 
          <i class="fa fa-coffee"></i>
          Bạn có thể sửa lại thông tin cá nhân sau!
        </div>
        <h3>Personal info</h3>
        
        
          <div class="form-group">
            <label class="col-lg-3 control-label">full name:</label>
            <div class="col-lg-8">
              <input class="form-control" name = "full_name" type="text" value="<?php echo $full_nameI ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">birday:</label>
            <div class="col-lg-8">
              <input class="form-control" name ="birday" type="date" value="<?php echo $birdayI ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-lg-3 control-label">address:</label>
            <div class="col-lg-8">
              <input class="form-control" name ="address" type="text" value="<?php echo $address ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" name = "email" type="text" value="<?php echo $emailI ?>">
            </div>
          </div>

          <button type="submit" name="edit_profile" class="btn btn-primary mb-2">Confirm</button>
<?php
    }
    if(isset($_POST['edit_profile'])){
      $full_name = $_POST["full_name"];
      $birday  = $_POST["birday"];
      $email = $_POST["email"];
      $address = $_POST["address"];

      // $up = move_uploaded_file($_FILES['txtpic']['tmp_name'], $_FILES['txtpic']['name']);

      // init user detail
      $dangki = new DangKi();
      $info = $dangki -> addInfo($full_name, $birday, $email, $address);
      // $idUser = 
      // $userAccount = new UserAccount();

      if($info == 1){
        ?>
        <div class="alert alert-success" role="alert">
          Sửa thành công!
        </div>
        <?php
          header("Refresh: 1; URL=edit_profile.php");
      }
    }
?>
        
      </div>
  </div>
  </form>
</div>
<hr>
</body>
</html>